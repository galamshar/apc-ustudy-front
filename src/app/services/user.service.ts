﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenStorageService } from './token-storage.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const API_URL = 'https://webapi20210729133449.azurewebsites.net/api/v1/users'

@Injectable({
    providedIn: 'root'
})

export class UserService {
    form: FormGroup;
    
    constructor(private http: HttpClient, private tokenService: TokenStorageService,private formBuilder: FormBuilder) { 
        this.form = this.formBuilder.group({
            login: this.formBuilder.control(null, Validators.required),
            role: this.formBuilder.control(null, Validators.required),
            regionId: this.formBuilder.control(null, Validators.required)
        })
    }

    async getUser(){
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        return await this.http.get(API_URL,{headers}).toPromise(); 
    }
    
    async addUser(regionId, username, role){
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        this.form.value.login = username
        this.form.value.role = role
        this.form.value.regionId = regionId
        this.http.post(API_URL,this.form.value, {headers}).subscribe(
            result => {
                return result
            },
            error => {
                return error.message
            }
        )
    }
}