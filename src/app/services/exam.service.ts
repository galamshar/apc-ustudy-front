﻿import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenStorageService } from './token-storage.service';

const API_URL = 'https://localhost:5001/api/v1/exams'

@Injectable({
    providedIn: 'root'
})

export class ExamService {
    constructor(private http: HttpClient, private tokenService: TokenStorageService) { }

    async getExams() {
        var headers = new HttpHeaders().set("Authorization", "Bearer " + this.tokenService.getToken())
        return await this.http.get(API_URL, { headers }).toPromise()
    }
}