﻿export class CalendarEvent{
    id:string;
    title:string;
    start:number;
    end:number;
    extendedProps:{};
}