import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router, private tokenStorageService: TokenStorageService, private userService: UserService) { }
  isLoginFailed = false;
  isLoggedIn = this.tokenStorageService.isLoggedIn();
  errorMessage = '';
  ngOnInit(): void {
    if(this.isLoggedIn){
      this.router.navigate(['/'])
    }
    this.form = this.formBuilder.group({
      login: '',
      password: ''
    })
  }

  submit(): void {
    this.http.post("https://webapi20210729133449.azurewebsites.net/api/v1/Authentication/token", this.form.getRawValue()).subscribe(
      async res => {
        this.tokenStorageService.saveToken(JSON.stringify(res))
        var myDate = new Date();
        myDate.setHours(myDate.getHours() + 1); //one hour from now
        localStorage.setItem('expiration', JSON.stringify(myDate));
        var user = await this.userService.getUser()
        this.tokenStorageService.saveUser(user)
        this.router.navigate(['/'])
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      })
  }
}
