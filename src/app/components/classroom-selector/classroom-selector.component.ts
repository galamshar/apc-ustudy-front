import { Component } from '@angular/core';

@Component({
  selector: 'app-classroom-selector',
  templateUrl: './classroom-selector.component.html',
  styleUrls: ['./classroom-selector.component.css']
})
export class ClassroomSelectorComponent implements OnInit {

  constructor() { }

  variants: any;
  selected = '';

}
