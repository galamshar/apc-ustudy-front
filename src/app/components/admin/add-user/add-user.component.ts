import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';
import { DialogData, NewEventDialogComponent } from '../../new-event-dialog/new-event-dialog.component';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  regions: any
  public form = new FormGroup({
    login: new FormControl(null, Validators.required),
    role: new FormControl(null, Validators.required),
    regionId: new FormControl(null, Validators.required)
  })
  roles = [
    {
      id: 1,
      name: "Внутренный клиент"
    },
    {
      id: 2,
      name: "Внешний клиент"
    }
  ]
  constructor(
    public dialogRef: MatDialogRef<NewEventDialogComponent>, private userService: UserService, @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {

  }

  ngOnInit(): void {
  }

  save() {
    let login = this.form.value.login;
    let role = this.form.value.role;
    let regionId = this.form.value.regionId;
    this.userService.addUser(regionId, login, role)
    this.dialogRef.close(this.form.value);
  }

  close() {
    console.log(this.form.value);
    this.dialogRef.close();
  }
}
