import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './components/signin/signin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScheduleComponent } from './components/schedule/schedule.component';

import { TokenStorageService } from './services/token-storage.service'
import { UserService } from './services/user.service'
import { OfficeService } from './services/office.service'
import { ExamService } from './services/exam.service'
import { EventService } from './services/event.service'
import { CalendarEvent } from './components/schedule/events'
import { ClassroomService } from './services/classroom.service';
import { NewEventDialogComponent } from './components/new-event-dialog/new-event-dialog.component'

import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { AdminComponent } from './components/admin/admin.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AddUserComponent } from './components/admin/add-user/add-user.component';
import { RegionSettingsComponent } from './components/region-settings/region-settings.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  resourceTimelinePlugin,
  timeGridPlugin,
  listPlugin,
]);

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ScheduleComponent,
    NewEventDialogComponent,
    AdminComponent,
    AddUserComponent,
    RegionSettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FullCalendarModule,
    HttpClientModule,
    MatDatepickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    NgxMatTimepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatMomentModule,
    MatPaginatorModule
  ],
  providers: [TokenStorageService, UserService, OfficeService, ExamService, EventService, CalendarEvent, ClassroomService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }